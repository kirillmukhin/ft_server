#!/bin/bash

# Set full path to the file that'll contain ngnix configuration:
folder_path="/etc/nginx/sites-available/"
file_name="default"
file_path="$folder_path$file_name"
# Record the number of the line, where "autoindex" is mentioned first to the variable.
# Keep in mind that script will not work as intended,
# if the first mention of "autoindex" isn't the directive
# (e.g. comment mentioning "autoindex" before the directive itself):
line_num="$(grep -n "autoindex" $file_path | head -n 1 | cut -d: -f1)"
# Record the contents of the entire line, where the word "autoindex" was mentioned
line_content=$(sed -n ${line_num}p ${file_path})

# Expected contents of the line, that we will compare 'line_content' to:
check_on="        autoindex on;"
check_off="        autoindex off;"

if [ "$line_content" == "$check_on" ]; then # if 'autoindex' enabled - disable it.
	sed -i "$line_num s/on/off/" $file_path
	nginx -s reload
	echo "[autoindex disabled]"
elif [ "$line_content" == "$check_off" ]; then # if 'autoindex' disabled - enable it.
	sed -i "$line_num s/off/on/" $file_path
	nginx -s reload
	echo "[autoindex enabled]"
else	# 'line_content' didn't match either expected line.
		# Maybe 'autoindex' directive is missing from the configuration entirely;
		# Or the word 'autoindex' was mentioned before the directive itself.
	>&2 echo "ERROR: 'autoindex' directive was not found in the '$file_path'"
fi
