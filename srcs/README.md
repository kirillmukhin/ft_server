# Build image:
Run command from the folder where the Dockerfile is located:

`docker build -t ft_server_img .`

- `-t` replaces randomly generated image name with the one following the flag (*ft_server_img* in this example).

This will create an image and assign it a name *ft_server_img*.

> [build docs](https://docs.docker.com/engine/reference/commandline/build/)

### Show all docker images
To check that building of an image was succesfull you can run:

`docker images`

You should see an image with the name you choose upon creation (*ft_server_img* in this example)

> [images docs](https://docs.docker.com/engine/reference/commandline/images/)

# Run image:

`docker run -it --name ft_server_ctr -p 80:80 -p 443:443 ft_server_img`

- `-it` — (`--interactive` + `--tty`) launch interactive shell upon starting the container.

- `--name` — assign name to the container.

- `-p` — publish a container’s ports to the host. Port 80 for HTTP, port 443 for HTTPS.

> [run docs](https://docs.docker.com/engine/reference/commandline/run/)

### Open shell inside the container:
If you'll want to open another shell instance inside the container you can use following command:

`docker exec -it ft_server_ctr zsh`

> [exec docs](https://docs.docker.com/engine/reference/commandline/exec/)

### Check containers:
If you want to list *all* avaliable containers you can following command 
(*run it from the system shell, not the container one*):

`docker ps -a`

To see only currently running containers - remove `-a` flag:

`docker ps`

> [ps docs](https://docs.docker.com/engine/reference/commandline/ps/)

### Stop container
You can *forcefully* stop the container by pressing `Ctrl + D` in the container's main shell (the one that was open after `docker run` command).

Alternatively you can *gracefully* stop the container by running from the system's (not container's) shell:

`docker stop ft_server_ctr`

> [stop docs](https://docs.docker.com/engine/reference/commandline/stop/)

### Relaunch container
After stopping the container you can relaunch it with:

`docker start ft_server_ctr`

Command above will *not* open an interactive shell inside the container. You'll still be able to access it with `docker exec` command, described above.

To start container and open shell inside of it right away you may use `-i` flag:

`docker start -i ft_server_ctr`

> [start docs](https://docs.docker.com/engine/reference/commandline/start/)

# Autoindex:
Make sure that server is up and running, then enter the shell inside the container and run:

`cd /var/www/html/ && mkdir autoindex_test && cd autoindex_test && touch file1 file2 file3`

Command above creates a new folder *autoindex_test* at the */var/www/html/* location. Three empty files are also created inside said folder.

In the browser open https://localhost/autoindex_test/

As autoindex is enabled by default (in the nginx configuration), the page should display contents of the created folder (file1, file2 and file3)

Back in the container's shell run toggle script:

`autoindex_toggle`

It'll check autoindex's state in the nginx config (*/etc/nginx/sites-available/default*) file and switch it to the opposite value (on/off).

After running the script refresh the page, now that autoindex is disabled, it should show *"403 Forbidden"* page.

Run the script again if you want to re-enable autoindex.


# Links
Check that all of the following links are working:
- https://localhost/ — nginx welcome page
- https://localhost/wordpress/ — website main page
- https://localhost/wordpress/wp-admin/ — WordPress admin dashboard
	- login:
		- jarnolf
	- password:
		- school21
- https://localhost/phpmyadmin/ — administration tool for MySQL(MariaDB).
	- login:
		- root
	- no password

I suggest you to try:
- Adding new comments on the website;
- Adding new posts through *wp-admin*:
	1. *Dashboard* on the left -> *Posts* -> *Add New*
	2. Add title, write some text -> Press *Publish* (twice)
- Editing existing comments through *wp-admin*:
	1. *Dashboard* -> *Comments*
	2. Hover over the text of any comment -> Press *Edit*
	3. Make some changes to the comment -> Press *Update*
- Editing existing posts and comments through the database, using *phpmyadmin*:
	1. On the left panel open *wordpress* database.
	2. Pick *wp_posts* or *wp_comments* table.
	3. If you are looking for post - find desired one by looking at *post_title* and *post_contet* columns. *post_status* should be *publish*.
	4. Press edit on the post/comment and add changes to the *post_content*/*comment_content* field.
	5. Press *Go* button at the bottom right corner of the page to apply the update.

Make sure to refresh the website and check every change you do through *wp-admin* or *phpmyadmin*.

# Remove container and image (debian image will be left in the system):
To remove both container and image in one line you can run:

`docker rm -f ft_server_ctr && docker rmi ft_server_img`

> [rm docs](https://docs.docker.com/engine/reference/commandline/rm/)

> [rmi docs](https://docs.docker.com/engine/reference/commandline/rmi/)
