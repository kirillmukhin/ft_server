-- https://bertvv.github.io/notes-to-self/2015/11/16/automating-mysql_secure_installation/

-- Setting the root password ensures that nobody can log into the MariaDB
-- root user without the proper authorisation.
-- UPDATE mysql.user SET Password=PASSWORD('school21') WHERE User='root';

-- By default, a MariaDB installation has an anonymous user, allowing anyone
-- to log into MariaDB without having to have a user account created for
-- them.  This is intended only for testing, and to make the installation
-- go a bit smoother.  You should remove them before moving into a
-- production environment.
DELETE FROM mysql.user WHERE User='';

-- Normally, root should only be allowed to connect from 'localhost'.
-- This ensures that someone cannot guess at the root password from the network.
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');

-- By default, MariaDB comes with a database named 'test' that anyone can
-- access.  This is also intended only for testing, and should be removed
-- before moving into a production environment.
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%';

-- Reloading the privilege tables will ensure that all changes made so far will take effect immediately.
FLUSH PRIVILEGES;
