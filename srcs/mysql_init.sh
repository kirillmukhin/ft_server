#!/bin/bash

service mysql start

mysql -u root --skip-password < /etc/docker_init/mysql_secure_installation.sql
mysql -u root --skip-password < /etc/docker_init/mysql_wordpress.sql
mysql wordpress -u root --skip-password < /etc/docker_init/wordpress.sql
